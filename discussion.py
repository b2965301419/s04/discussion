# Python Pillars of OOP

class Bootcamper:
	def __init__(self, name, batch):
		self.name = name
		self.batch = batch

	def info(self):
		print(f"My name is {self.name} of batch {self.batch}")

zuitt_camper = Bootcamper('Rahul', 242)
print(f'Bootcamper name: {zuitt_camper.name}')
print(f'Bootcamper batch: {zuitt_camper.batch}')
zuitt_camper.info()

# [Section] Encapsulation
# Encapsulation is a mechanism of wrapping the attributes and code acting on the methods together as a single unit

# To achieve encapsulation
# Declare the attributes of a class
# Provide the getters and setters to modify and view the attributes values

class Person():
	def __init__(self):
		# Prefix underscore is used as a warning for the developers that the property should not be accessed in any way outside its declared class
		self._name = "Bruno Mars"
		self._age = 37

	# Getter
	def get_name(self):
		print(f'Name of the person: {self._name}')
	def get_age(self):
		print(f'Age of the person: {self._age}')

	# Setter
	def set_name(self, name):
		self._name = name
	def set_age(self, age):
		self._age = age

# Why encapsulation?
# The Fields of a class can be made to read-only and/or write-only
p1 = Person()
p1.get_name()
# print(p1._name)
# print(p1.name)
p1.set_name('Justin Bieber')
p1.get_name()
p1.get_age()
p1.set_age(29)
p1.get_age()

# [Section] Inheritance
# Transfer of characteristics of a parent class to a child class that is derived from it
class Employee(Person):
	def __init__(self, employee_id):
		super().__init__()
		self._employee_id = employee_id

	# Getters
	def get_employee_id(self):
		print(f'The id of the employee is {self._employee_id}')

	# Setters
	def set_employee_id(self, employee_id):
		self._employee_id = employee_id
	
	def get_details(self):
		print(f'{self._employee_id} belongs to {self._name}')

emp1 = Employee('001')
emp1.get_employee_id()
emp1.get_details()
emp1.get_name()
emp1.set_name('JB')
emp1.get_name()
emp1.get_details()

class Student(Person):
	def __init__(self, student_no, course, year_level):
		super().__init__()
		self._student_no = student_no
		self._course = course
		self._year_level = year_level

	# Getters
	def get_student_no(self):
		print(f'The number of the student is {self._student_no}')
	def get_course(self):
		print(f'The course of the student is {self._course}')
	def get_year_level(self):
		print(f'The year level of the student is {self._year_level}')
	
	# Setters
	def set_student_no(self, student_no):
		self._student_no = student_no
	def set_course(self, course):
		self._course = course
	def set_year_level(self, year_level):
		self._year_level = year_level

	def get_details(self):
		print(f'{self._name} is currently in year {self._year_level} taking up {self._course}')
		
std1 = Student(192, 'CS452', '3')
std1.get_details()

# [Section] Polymorphism
# A child class inherits all methods from the parent class. However, in some situations, the method inherited from the parent class doesn't quite fit into child class. In such cases, we will have to re-implement the

# Functions and objects
class Admin():
	def is_admin(self):
		print(True)
	def user_type(self):
		print('Admin User')

class Customer():
	def is_admin(self):
		print(False)
	def user_type(self):
		print('Regular User')

# Define a test function that will take an object called obj
def test_function(obj):
	obj.is_admin()
	obj.user_type()

# The test_function would call the methods of the object passed to it, hence allowing them to have different outputs, depending on the object
user_admin = Admin()
user_reg = Customer()
test_function(user_admin)
test_function(user_reg)

class TeamLead():
	def occupation(self):
		print('Team Lead')
	def hasA_auth(self):
		print(True)
class TeamMember():
	def occupation(self):
		print('Team Member')
	def hasA_auth(self):
		print(False)

tl1 = TeamLead()
tm1 = TeamMember()

for person in (tl1, tm1):
	person.occupation()
	person.hasA_auth()
# The person is assigned as tl1 or tm1 depending on the sequence of the loop

# Polymorphism with inheretence
# Polymorphism defined methods in the parent class
# In Inheritence, the child class inherits from the parents. Also, it is possible to modify a method in a child class that was inherited from the parent class
class Zuitt():
	def tracks(self):
		print('We are currently offering 3 tracks: developer career, pi-share career, and short courses')
	def num_of_hours(self):
		print('Learn web development in 360 hours')
class DeveloperCareer(Zuitt):
	def num_of_hours(self):
		print('Learn web development in 220 hours')
class PiShapeCareer(Zuitt):
	def num_of_hours(self):
		print('Learn web development in 120 hours')
class ShortCourses(Zuitt):
	def num_of_hours(self):
		print('Learn web development in 20 hours')

course1 = DeveloperCareer()
course2 = PiShapeCareer()
course3 = ShortCourses()
print("---------------------------------")
course1.num_of_hours()
course2.num_of_hours()
course3.num_of_hours()

# or
for course in (course1, course2, course3):
	print("----------------------------------")
	course.num_of_hours()

print("----------------------------------")
# [Section] Abstraction
# Abstract class can be considered as a blueprint for other classes. It allows us to create a set of method that must be created within any child class

# Abstract method is a method that has a declaration but does not have an implementation
# The implementation of abstract classes is to be defined in each child class
# By default, python does not have a built-in package for abstract methods/abstract classes
# Python does come with a module that provides the base for defining Abstract Base Classes (ABC)
from abc import ABC, abstractmethod
# ABC directive creates abstract base classes
class Polygon(ABC):
	# @abstractmethod decorator creates an abstract method that needs to be implemented by the child classes the inherit Polygon class
	@abstractmethod
	def print_number_of_sides(self):
		# pass keyword that the method doesn't do anything
		pass

class Triangle(Polygon):
	def __init__(self):
		super().__init__()
	def print_number_of_sides(self):
		print(f'This Polygon has 3 sides')

class Pentagon(Polygon):
	def __init__(self):
		super().__init__()
	def print_number_of_sides(self):
		print(f'This Polygon has 5 sides')

shape1 = Triangle()
shape2 = Pentagon()

for shape in (shape1, shape2):
	shape.print_number_of_sides()